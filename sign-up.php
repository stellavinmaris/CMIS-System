<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CMIS | System</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="resources/img/favicon.ico" />
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="vendor/iCheck/all.css" /> 
        <!-- Icons -->
        <link href="resources/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="resources/icons/themify-icons/themify-icons.css" rel="stylesheet">
        <!-- Theme style -->
        <link rel="stylesheet" href="resources/css/main-style.min.css">
        <link rel="stylesheet" href="resources/css/skins/all-skins.min.css">
        <link rel="stylesheet" href="resources/css/demo.css">
  
</head>
    <body class="skin-blue register-page">
        <div class="box-register">
            <div class="box-register-body">
                <h3><span><b>CMI-</b>System</span></h3>
                <p class="box-register-msg">Sign in to start your society registration</p>

                <?php
        
                    include'conn.php';
                    $errors = array();
                    if(isset($_POST['submit']))
                    {
                    $username=trim(strip_tags($_POST['username']));
                    $pwd=trim(strip_tags($_POST['pwd']));
                    $cpwd=trim(strip_tags($_POST['cpwd']));
                    $email=trim(strip_tags($_POST['email']));
                    $status=trim(strip_tags($_POST['status']));
                    $encrypted_md5_password = md5($pwd);

                    $username=stripslashes($username);
                    $pwd=stripslashes($pwd);
                    $cpwd=stripslashes($cpwd);
                    $email=stripslashes($email);
                    $status=stripslashes($status);
                    $encrypted_md5_password =stripslashes($encrypted_md5_password);

                    $username=mysql_real_escape_string($username);
                    $pwd=mysql_real_escape_string($pwd);
                    $cpwd=mysql_real_escape_string($cpwd);
                    $email=mysql_real_escape_string($email);
                    $status=mysql_real_escape_string($status);
                    $encrypted_md5_password =mysql_real_escape_string($encrypted_md5_password);

                    $check_for_duplicates = mysql_query("select * from users where email='$email'");

                     $results = mysql_num_rows($check_for_duplicates);


                if(preg_match("/\S+/", $pwd)===0 || strlen($pwd) <= 7)
                  {
                    $errors["pwd"] = "* Password must be at least 8 characters";
                  }
                  
                  elseif($cpwd !== $pwd )
                  {
                    $errors["cpwd"] = "* Password must match";
                  }

                  elseif($results > 0)
                    {
                        echo "<center><p style=color:red;>Sorry, your email address already exist in our database and duplicate email addresses are not allowed for security reasons.<br>Please enter a different email address to proceed or login with your existing account. Thanks</p></center>";
                        
                    }


                 
                  else
                  {
                  if(mysql_query("insert into `users` values('', '".mysql_real_escape_string($username)."', '".mysql_real_escape_string($email)."', '".mysql_real_escape_string($encrypted_md5_password)."', '".mysql_real_escape_string(date('Y-m-d'))."',
                    '".mysql_real_escape_string($status)."')"))
                    {
                        $_SESSION["email"] = $email;
                        $_SESSION["username"] = strip_tags($username);

                         echo "<script> alert('Account was successfully created');
                        window.location.href='index.php';
                        </script>";
                        // header("location: index.php");
                    }
                        else
                            echo "<center><p style=color:red;>Sorry, your account could not be created at the moment. Please try again or contact the site admin to report this error if the problem persist. Thanks.<p></center>";

                  }
                 
                }
                
                
            ?>

                <form class="register-form" method="post" action="">
                   
                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    
                        <input class="form-control" type="text" name='username' placeholder="Username" required />

                    </div>


                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        
                        <input class="form-control" type="email" name='email' placeholder="Email" required />
                    </div>

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                       
                        <input class="form-control" type="password" name='pwd' placeholder="Password" required />
                    </div>

                    <input class="form-control" type="text" style="display: none;" name='status' id="status" value="society" required />

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <span class="error">
                            <?php
                                if(isset($errors["cpwd"]))
                                {
                                  echo $errors["cpwd"];
                                }
                              ?>
                        </span>
                        <input class="form-control" type="password" name='cpwd' placeholder="Confirm Password" required />
                    </div>

                   
                    <div class="form-group form-action">
                        <button type="submit" name="submit"  class="btn btn-block btn-default">Sign Up</button>
                    </div>

                    <div class="form-group text-center">
                        <a href="index.html">I already have a membership</a>&nbsp;|&nbsp;<a href="#">Support</a>
                    </div>
                </form>
            </div>
        </div>
		
        <!-- JS scripts -->
        <script src="vendor/jQuery/jquery-2.2.3.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="resources/js/pages/jquery-icheck.js"></script>
        <script src="vendor/fastclick/fastclick.min.js"></script>
		<script src="resources/js/demo.js"></script>
    </body>
</html>