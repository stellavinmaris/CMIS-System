
<?php
    include 'finance-header.php';
?>
            <!-- Content Wrapper -->
            <div class="content-wrapper">
                <section class="content-title">
                    <h1>
                        Approved Loans
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i>Dashboard</a></li>
                        
                        <li class="active">Approved Loans</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Approved Loans</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            


                              <table class="table responsive datatable">
            <thead>
                <tr>
                    
                    <th>Date of Application</th>
                    <th>Society Name</th>
                    <th>Bank Name</th>
                    <th>Bank Account</th>
                    <th>Amount</th>
                    <th>Status</th>
                    
                </tr>
            </thead>
            <tbody>


<?php
        
        include'conn.php';
        //$errors = array();
        // $id=$_REQUEST['societyId'];
        // echo "<script>console.log('object".$id."');</script>";
        $result=mysql_query("select * from loans");
        // $test=mysql_fetch_array($result);
        
        // echo "<script>console.log('object".$id."');</script>";
        if(!$result)
        {
                die("data not found");
        }
        while($row=mysql_fetch_array($result))
        {
        // $id = $row['memberId'];

               echo " <tr>";
               
                  echo  '<td>'.$row['dateOfApplication'].'</td>';
                   echo '<td>'.$row['societyName'].'</td>';
                    echo '<td>'.$row['bankName'].'</td>';
                    echo '<td>'.$row['bankAccount'].'</td>';
                    echo '<td>'.$row['amount'].'</td>';
                    echo "<td><span class='label label-success'>Approved</span></td>";

                    
               echo ' </tr>';
            }
                ?>
               
            </tbody>
        </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </section>
                <!-- /. main content -->
                <span class="return-up"><i class="fa fa-chevron-up"></i></span>
            </div>
            <!-- /. content-wrapper -->
            <!-- Main Footer -->
           <footer class="main-footer">
<!-- Default to the left -->
<strong>Copyright &copy; 2018 <a href="#">CMIS</a>.</strong> All rights reserved.
<!-- To the right -->
<div class="pull-right hidden-xs"></div>
</footer>
        </div>

        <!-- /. wrapper content-->
        <!-- JS scripts -->
        <script src="vendor/jQuery/jquery-2.2.3.min.js"></script>
        <script src="vendor/jquery-fullscreen/jquery.fullscreen-min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/fastclick/fastclick.min.js"></script>
        <!-- DataTables -->
        <script src="vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="vendor/datatables/dataTables.bootstrap.min.js"></script>
        <script src="vendor/responsive-tables/responsivetables.js"></script>
        <script src="resources/js/pages/jquery-datatable.js"></script>
        <script src="resources/js/app.min.js"></script>
        <script src="resources/js/demo.js"></script>
        <!-- Slimscroll is required when using the fixed layout. -->
    </body>
</html>