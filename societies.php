<?php
    include 'admin-header.php';
?>
<!-- Content Wrapper -->
<div class="content-wrapper">
<section class="content-title">
<h1>
    All Societies
    <small></small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i>Dashboard</a></li>
   
    <li class="active">All Societies</li>
</ol>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
    <?php 
    include "conn.php";

    // $userid = $_SESSION["userId"];


    $result =mysql_query( "select * from societies ");
    while($row=mysql_fetch_array($result))
    {
        $id = $row['societyId'];
        echo "<script>console.log('object:".$id."');</script>";
    echo "<div class='col-lg-3 col-md-4 col-sm-6'>";
        echo "<div class='box all-doctors'>";
            echo "<div class='box-body'>";
                // echo '<img class="member-offline img-circle" src="resources/img/icons/icon-user4.png" alt="User Image">';
            echo "<br>";echo "<br>";echo "<br>";
                echo '<p class="name">'.$row['name'].'</p>';
                 echo '<div class="information">';
                    echo '<p class=""> Area Of Operation:&nbsp;&nbsp;' .$row['areaOfOperation'].'</p>';
                    echo '<p class="email"><a href="#"> Email : &nbsp;&nbsp;'.$row['email'].'</a>
                    </p>';
                    echo '<p class="location text-muted"> Address : &nbsp;&nbsp;'.$row['address'].'</p>';
               echo  '</div>
                <div class="text-center action-profile">
                    <a href="admin-society-profile.php?societyId='.$id.'" class="btn btn-success">View Profile</a>

                    <a href="admin-edit-society.php?societyId='.$id.'" class="btn btn-danger">Edit Profile</a>

                    <a href="admin-view-society-members.php?societyId='.$id.'" class="btn btn-primary">view Members</a>
                    
                </div>';
                echo "<br>";echo "<br>";echo "<br>";
               echo '
            </div>
        </div>
    </div>';
    }
    ?>  


</div>
</section>
    <!-- /. main content -->
    <a href="add-society.html" class="add-icon"><i class="fa fa-plus"></i></a>
    <span class="return-up"><i class="fa fa-chevron-up"></i></span>
</div>
           <footer class="main-footer">
<!-- Default to the left -->
<strong>Copyright &copy; 2018 <a href="#">CMIS</a>.</strong> All rights reserved.
<!-- To the right -->
<div class="pull-right hidden-xs"></div>
</footer>
</div>

<!-- /. wrapper content-->
<!-- JS scripts -->
<script src="vendor/jQuery/jquery-2.2.3.min.js"></script>
<script src="vendor/jquery-fullscreen/jquery.fullscreen-min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/fastclick/fastclick.min.js"></script>
<script src="vendor/chartjs/Chart.min.js"></script>
<script src="vendor/sparkline/jquery.sparkline.min.js"></script>
<script src="resources/js/app.min.js"></script>
<script src="resources/js/demo.js"></script>
<script src="resources/js/pages/dashboard.js"></script>
<!-- Slimscroll is required when using the fixed layout. -->
</body>
</html>