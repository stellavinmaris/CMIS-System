
<?php
//Start session
ob_start();
session_start();
 
if(!isset($_SESSION['userId']) || (trim($_SESSION['username']) == '')) {
    header("location: index.php");
    exit();
}
?>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>CMIS Admin</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" href="resources/img/favicon.ico" />
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
<!-- Icons -->
<link href="resources/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="resources/icons/themify-icons/themify-icons.css" rel="stylesheet">
<!--animate css-->
<link rel="stylesheet" href="vendor/animate.css">
<!-- Theme style -->
<link rel="stylesheet" href="resources/css/main-style.min.css">
<link rel="stylesheet" href="resources/css/skins/all-skins.min.css">
<link rel="stylesheet" href="resources/css/demo.css">

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112423372-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-112423372-1');
</script>
</head>

<body class="skin-blue sidebar-mini">

<div class="wrapper">
<!-- Main Header -->
<header class="top-menu-header">
<!-- Logo -->
<a href="dashboard.php" class="logo">
<!-- mini logo for sidebar mini 50x50 pixels -->
<span class="logo-mini"><img src="resources/img/logo-mini.png" class="img-circle" alt="Logo Mini"/></span>
<!-- logo for regular state and mobile devices -->
<span class="logo-lg"><b>CMIS</b>Admin</span>
</a>
<!-- Header Navbar -->
<nav class="navbar navbar-static-top">
<!-- Sidebar toggle button-->
<a class="sidebar-toggle fa-icon" data-toggle="offcanvas" role="button">
<span class="sr-only">Toggle navigation</span>
</a>
<!-- Navbar Right Menu -->
<div class="navbar-top-menu">
<ul class="nav navbar-nav">
    <!-- Navbar Search -->
    <li>
        <a data-toggle="collapse" data-target="#top-menu-navbar-search" aria-expanded="false">
            <i class="fa fa-search"></i>
        </a>
    </li>
    
  
    <li class="dropdown user user-menu">
        <!-- Menu Toggle Button -->
        <a href="#" data-toggle="dropdown" aria-expanded="false">
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs">Welcome <?php echo $_SESSION["username"]; ?><i class="fa fa-angle-down pull-right"></i></span>
            <!-- The user image in the navbar-->
            <img src="resources/img/icons/icon-user.png" class="user-image" alt="User Image">
        </a>
        <ul class="dropdown-menu user-menu animated flipInY">
            
            
            <li class="divider"></li>
            <li><a href="logout.php"><i class="ti-power-off"></i> Log Out</a></li>
        </ul>
    </li>
</ul>
<!-- Form Navbar Search -->
<div class="collapse top-menu-navbar-search" id="top-menu-navbar-search">
    <form>
        <div class="form-group">
            <div class="input-search">
                <div class="input-group">
                    <input type="text" id="navbar-search" name="search" class="form-control" placeholder="Search">
                    <span class="input-group-addon">
                        <a data-target="#top-menu-navbar-search" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="fa fa-times"></i></a>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /. Form Navar Search -->
</div>
</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="sidebar-left">
<section class="sidebar">
<!-- Sidebar user panel -->
<div class="user-panel">
<div class="pull-left image">
    <img src="resources/img/icons/icon-user.png" class="img-circle" alt="User Image">
</div>
<div class="info">
    <p> <?php echo $_SESSION["username"];?></p>
    <p><small><?php echo $_SESSION["email"];?></small>
    <p><small><?php echo $_SESSION["status"];?></small>
    </p>
    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
</div>

</div>
<!-- Sidebar Menu -->
<ul class="sidebar-menu">
<li class="header">MAIN NAVIGATION</li>
<li class="treeview active"><a href="dashboard.php"><i class="fa fa-home"></i> <span>Dashboard</span></a>
</li>
<li class="treeview"><a href="societies.php"><i class="fa fa-user-md"></i> <span>View Societies</span></a>
</li>

<li class="treeview"><a href="adminminutes.php"><i class="fa fa-user-md"></i> <span>View Minutes </span></a>
</li>

<li class="treeview"><a href="societies-payment.php"><i class="fa fa-usd"></i> <span> View Payment Details </span></a>
</li>

<!-- <li class="treeview"><a href="add-payment.php"><i class="fa fa-usd"></i> <span>Add Society Payment </span></a>
</li> -->

<li class="treeview"><a href="add-society.php "><i class="fa fa-user-md"></i> <span>Add Society </span></a>
</li>


<li class="treeview"><a href="logout.php"><i class="ti-power-off"></i> <span>Logout </span></a>
</li>




</ul>
<!-- /. sidebar-menu -->
</section>
</aside>

