-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 08, 2018 at 08:59 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cmis`
--

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` int(11) NOT NULL,
  `dateOfApplication` varchar(100) NOT NULL,
  `societyName` varchar(100) NOT NULL,
  `bankName` varchar(100) NOT NULL,
  `bankAccount` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loans`
--

INSERT INTO `loans` (`id`, `dateOfApplication`, `societyName`, `bankName`, `bankAccount`, `amount`, `status`) VALUES
(3, '2018-02-01', 'Kisumu society', 'Equity', '75845', '64563', ''),
(5, '2018-02-01', 'Kisumu society', 'Equity', '897654', '23456789', ''),
(6, '2018-02-01', 'Nairobi Society', 'KCB', '584674', '675346753', ''),
(7, '2018-02-01', 'Thika Society', 'Equity', '53564578680', '100', ''),
(8, '2018-02-01', 'Nairobi Society', 'KCB', '45431222', '45546', ''),
(9, '2018-02-01', 'Nairobi Society', 'u9087868096', '57645653', '080978', ''),
(10, '2018-02-01', 'Thika Society', '89698569', '79086890', '868969', ''),
(11, '2018-02-01', 'Thika Society', '98765432647', '23456789', '9867', ''),
(12, '2018-02-01', 'Thika Society', '576478', '456784', '785894', '');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `memberId` int(11) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `societyId` int(11) NOT NULL,
  `yob` varchar(100) NOT NULL,
  `gander` varchar(100) NOT NULL,
  `mobileNo` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `representative` varchar(100) NOT NULL,
  `relationship` varchar(100) NOT NULL,
  `nationalid` varchar(100) NOT NULL,
  `regDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`memberId`, `firstName`, `lastName`, `occupation`, `societyId`, `yob`, `gander`, `mobileNo`, `email`, `address`, `representative`, `relationship`, `nationalid`, `regDate`) VALUES
(6, 'stella', 'sikhila', 'software enginer', 6, '1992', 'female', '87986785764', 'd@gmail.com', '6454-7575 Nairobi', 'Paul', 'workmate', '74676467478', '2018-02-01'),
(7, 'Rhoda', 'Kimanga', 'HR', 6, '1998', 'female', '64564674', 'f@gmail.com', '6758-643 kisumu', 'Paul', 'workmate', '645673475683', '2018-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `minutes`
--

CREATE TABLE `minutes` (
  `minutesId` int(11) NOT NULL,
  `society` varchar(100) NOT NULL,
  `minutes` blob NOT NULL,
  `date` date NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `minutes`
--

INSERT INTO `minutes` (`minutesId`, `society`, `minutes`, `date`, `userId`) VALUES
(1, 'Thika Society', 0x7265672d6c6f67696e2d6164642d7570646174652d64656c6574652e7a6970, '2018-02-06', 8),
(2, 'Thika Society', 0x73616d706c652e7a6970, '2018-02-06', 8),
(3, 'Thika Society', 0x73747564656e742e7a6970, '2018-02-06', 8),
(4, 'Thika Society', 0x7365636c6f67616e64726567202831292e7a6970, '2018-02-06', 8),
(5, 'Nairobi Society', 0x31382e6a706567, '2018-02-06', 8);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `paymentId` int(11) NOT NULL,
  `societyId` int(11) NOT NULL,
  `amount` varchar(250) NOT NULL,
  `modeOfPayment` varchar(250) NOT NULL,
  `transactionCode` varchar(250) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`paymentId`, `societyId`, `amount`, `modeOfPayment`, `transactionCode`, `status`, `date`) VALUES
(3, 6, '86u989775', 'Mpesa', 'sdfghjklhjfjhfkl', '0', 2018),
(4, 7, '678587', 'Cheque', 'gjkhklhlkghylk', '0', 2018),
(5, 8, '436346', 'Mpesa', 'jhkgkhghjfhj', 'Paid', 2018);

-- --------------------------------------------------------

--
-- Table structure for table `societies`
--

CREATE TABLE `societies` (
  `societyId` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `areaOfOperation` varchar(250) NOT NULL,
  `bankName` varchar(250) NOT NULL,
  `bankAccount` varchar(250) NOT NULL,
  `regDate` varchar(250) NOT NULL,
  `langOfBooks` varchar(250) NOT NULL,
  `mobileNo` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `userId` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `paymentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `societies`
--

INSERT INTO `societies` (`societyId`, `name`, `areaOfOperation`, `bankName`, `bankAccount`, `regDate`, `langOfBooks`, `mobileNo`, `email`, `address`, `userId`, `memberId`, `paymentId`) VALUES
(6, 'Thika Society', 'kakamega 3', 'KCB', '5453', '2018-01-31', '--', '87986785764', 'janu@yahoo.com', '6454-7575 Nairobi', 8, 0, 0),
(7, 'Nairobi Society', 'Nairobi', 'KCB', '76755', '2018-01-31', '--', '87986785764', 'tyryu@yahoo.com', '6454-7575 Nairobi', 8, 0, 0),
(8, 'mombasa society', 'mombasa', 'DTB', '53564578680', '2018-02-02', 'Kiswahili', '866975764674', 'mum@gmail.com', '6454-7575 mombasa', 6, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `username`, `email`, `password`, `date`, `status`) VALUES
(1, 'stellavinmaris', 'stellavin@gmail.com', 'chyle1992', '0000-00-00', ''),
(2, 'stellavinmaris', 'd@gmail.com', 'chyle1992', '0000-00-00', ''),
(3, 'maris', 'stellavin@gmail.com', 'chyle1992', '0000-00-00', ''),
(4, 'maris', 'stellavin@gmail.com', 'chyle1992', '0000-00-00', ''),
(5, 'rk', 'janu@yahoo.com', 'e2052be51729c782bf551ec362d72f02', '2018-01-31', 'finance'),
(6, 'mum', 'mum@gmail.com', 'e2052be51729c782bf551ec362d72f02', '2018-01-31', 'admin'),
(7, 'redirect', 'redirect@gmail.com', '25d55ad283aa400af464c76d713c07ad', '2018-01-31', ''),
(8, 'faith', 'f@gmail.com', 'e2052be51729c782bf551ec362d72f02', '2018-01-31', 'society');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`memberId`),
  ADD KEY `societyId` (`societyId`);

--
-- Indexes for table `minutes`
--
ALTER TABLE `minutes`
  ADD PRIMARY KEY (`minutesId`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD KEY `paymentId` (`paymentId`),
  ADD KEY `societyId` (`societyId`);

--
-- Indexes for table `societies`
--
ALTER TABLE `societies`
  ADD PRIMARY KEY (`societyId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `memberId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `minutes`
--
ALTER TABLE `minutes`
  MODIFY `minutesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `paymentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `societies`
--
ALTER TABLE `societies`
  MODIFY `societyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_ibfk_1` FOREIGN KEY (`societyId`) REFERENCES `societies` (`societyId`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`societyId`) REFERENCES `societies` (`societyId`);

--
-- Constraints for table `societies`
--
ALTER TABLE `societies`
  ADD CONSTRAINT `societies_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
