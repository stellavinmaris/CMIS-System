<?php
    include 'society-header.php';
?>



<!-- Content Wrapper -->
<div class="content-wrapper">
<section class="content-title">
<h1>
<!-- Welcome to CMIS Admin Dashboard -->
<small></small>
</h1>
<ol class="breadcrumb">
<li><a href="#"><i class="fa fa-home"></i>Payment Methods</a></li>
</ol>
</section>

<section class="content mainpadding">
<p><?php echo "Today is".  "  ". date("M/d/y") . " - " . date("l") . ""?></p>
<p><?php echo "" . date("h:i:sa") . ""?></p>
<div class="row">
<a href="mpesa.php">
<div class="col-sm-6 col-lg-3">

    <div class="info-box">
        <div class="info-box-content">
            <i class="fa fa-usd text-navy"></i>
            <div class="text-center value">Mpesa</div>
        </div>
    </div>
</div>
</a>
<!--/.col-->
<a href="bank.php">
<div class="col-sm-6 col-lg-3">
    <div class="info-box">
        <div class="info-box-content">
            <i class="fa fa-usd text-light-blue"></i>
            <div class="text-center value">KCB BANK</div>
            
        </div>
    </div>
</div>
</a>
<!--/.col-->
<a href="bank.php">
<div class="col-sm-6 col-lg-3">
    <div class="info-box">
        <div class="info-box-content">
            <i class="fa fa-usd text-light-blue"></i>
            <div class="text-center value">EQUITY BANK</div>
           
        </div>
    </div>
</div>
</a>
<!--/.col-->
<a href="bank.php">
<div class="col-sm-6 col-lg-3">
    <div class="info-box">
        <div class="info-box-content">
            <i class="fa fa-usd text-green"></i>
            <div class="text-center value">BACKLAYS BANK</div>
            
        </div>
    </div>
</div>
</a>
<!--/.col-->
</div>



</section>
<!-- /. main content -->
<span class="return-up"><i class="fa fa-chevron-up"></i></span>
</div>
<!-- /. content-wrapper -->
<!-- Main Footer -->
<footer class="main-footer">
<!-- Default to the left -->
<strong>Copyright &copy; 2018 <a href="#">CMIS</a>.</strong> All rights reserved.
<!-- To the right -->
<div class="pull-right hidden-xs"></div>
</footer>
</div>

<!-- /. wrapper content-->
<!-- JS scripts -->
<script src="vendor/jQuery/jquery-2.2.3.min.js"></script>
<script src="vendor/jquery-fullscreen/jquery.fullscreen-min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/fastclick/fastclick.min.js"></script>
<script src="vendor/chartjs/Chart.min.js"></script>
<script src="vendor/sparkline/jquery.sparkline.min.js"></script>
<script src="resources/js/app.min.js"></script>
<script src="resources/js/demo.js"></script>
<script src="resources/js/pages/dashboard.js"></script>
<!-- Slimscroll is required when using the fixed layout. -->
</body>
</html>