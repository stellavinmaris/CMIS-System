<?php
    include_once 'admin-header.php';

        
                    include'conn.php';
                    $errors = array();
                    if(isset($_POST['submit']))
                    {
                    $name=trim(strip_tags($_POST['name']));
                    $areaOfOperation=trim(strip_tags($_POST['areaOfOperation']));
                    $bankName=trim(strip_tags($_POST['bankName']));
                    $bankAccount=trim(strip_tags($_POST['bankAccount']));
                    
                    $langOfBooks = trim(strip_tags($_POST['langOfBooks']));
                    $mobileNo=trim(strip_tags($_POST['mobileNo']));
                    $email=trim(strip_tags($_POST['email']));
                    $address=trim(strip_tags($_POST['address']));
                    $user=trim(strip_tags($_SESSION["userId"]));

                    $name=stripslashes($name);
                    $user=stripslashes($user);
                    $areaOfOperation=stripslashes($areaOfOperation);
                    $bankName=stripslashes($bankName);
                    $bankAccount=stripslashes($bankAccount);
                    
                    $langOfBooks =stripslashes($langOfBooks);
                    $mobileNo=stripslashes($mobileNo);
                    $email=stripslashes($email);
                    $address =stripslashes($address);

                    $name=mysql_real_escape_string($name);
                     $user=mysql_real_escape_string($user);
                    $areaOfOperation=mysql_real_escape_string($areaOfOperation);
                    $bankName=mysql_real_escape_string($bankName);
                    $bankAccount=mysql_real_escape_string($bankAccount);
                    
                    $langOfBooks =mysql_real_escape_string($langOfBooks);
                    $mobileNo=mysql_real_escape_string($mobileNo);
                    $email=mysql_real_escape_string($email);
                    $address =mysql_real_escape_string($address);

                    $check_for_duplicates = mysql_query("select * from societies where email='$email'");

                     $results = mysql_num_rows($check_for_duplicates);


                if($results > 0)
                    {
                        echo "<center><p style=color:red;>Sorry, your society address already exist in our database and duplicate  societies are not allowed for security reasons.<br>Please enter a different email address to proceed . Thanks</p></center>";
                        
                    }


                 
                  else
                  {
                  if(mysql_query("insert into `societies` values('', '".mysql_real_escape_string($name)."', 
                '".mysql_real_escape_string($areaOfOperation)."', 
                '".mysql_real_escape_string($bankName)."',
                  '".mysql_real_escape_string($bankAccount)."', 
                  '".mysql_real_escape_string(date('Y-m-d'))."',
                  '".mysql_real_escape_string($langOfBooks)."',
                  '".mysql_real_escape_string($mobileNo)."',
                  '".mysql_real_escape_string($email)."',
                    '".mysql_real_escape_string($address)."'
                    ,
                    '".mysql_real_escape_string($user)."',
                    '".mysql_real_escape_string('0')."',
                    '".mysql_real_escape_string('0')."')"))
                    {
                        // $_SESSION["email"] = $email;
                        // $_SESSION["username"] = strip_tags($username);
                        echo "<script> alert('Society was added successfully');
                        window.location.href='add-society.php';
                        </script>";
                        // header("location: add-society.php");
                    }
                        else
                            // echo "<center><p style=color:red;>Sorry, your society account could not be created at the moment. Please try again or contact the site admin to report this error if the problem persist. Thanks.<p></center>";
                            die(mysql_error());


                  }
                 
                }
                
                
            ?>

            <!-- Content Wrapper -->
            <div class="content-wrapper">
                <section class="content-title">
                    <h1>
                        Add Society
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i>Dashboard</a></li>
                        
                        <li class="active">Add Society</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                <form method="post" action="">
                    <div class="box box-form">
                        <div class="box-header">
                            <h3 class="box-title">Basic Information</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class='row'>
                                    <div class='col-md-6'>
                                        <div class='form-group'>
                                            <label>Name</label>
                                            <input class="form-control" id="name" name="name" type="text" />
                                        </div>
                                    </div>
                                    <div class='col-md-6'>
                                        <div class='form-group'>
                                            <label>Area of Operation</label>
                                            <input class="form-control" id="areaOfOperation" name="areaOfOperation" type="text" />
                                        </div>
                                    </div>
                                </div>
                                 <div class='row'>
                                    <div class='col-md-6'>
                                        <div class='form-group'>
                                            <label>Bank Name</label>
                                            <input class="form-control" id="bankName" name="bankName" type="text" />
                                        </div>
                                    </div>

                                    <div class='col-md-6'>

                                        <div class='form-group'>
                                            <label>Bank Account</label>
                                            <input class="form-control" id="bankAccount" name="bankAccount" type="text" />
                                        </div>
                                        </div>
                                    </div>
                                <div class='row'>
                                    <!-- <div class='col-md-4'>
                                        <div class='form-group'>
                                            <label>Registration Date</label>
                                            <input class="form-control pickadate" id="regDate" name="regDate" type="text" placeholder="Select date"/>
                                        </div>
                                    </div> -->
                                    <div class='col-md-6'>
                                        <div class="form-group">
                                            <label>Language of books to be recorded</label>
                                            <select class="form-control" name="langOfBooks">
                                                <option>--</option>
                                                <option value="English">English</option>
                                                <option value="Kiswahili">Kiswahili</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-6'>
                                        <div class='form-group'>
                                            <label>Mobile Number</label>
                                            <input class="form-control" id="mobileNo" name="mobileNo" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-6'>
                                        <div class='form-group'>
                                            <label>Email</label>
                                            <input class="form-control" id="email" name="email" type="email" />
                                        </div>
                                    </div>

                                    <div class='col-md-6'>

                                        <div class='form-group'>
                                            <label>Postal Address</label>
                                            <input class="form-control" id="address" name="address" type="text" />
                                        </div>
                                        </div>
                                    </div>
                                  
                                </div>
                              
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    </form>
                   
                </section>
                <!-- /. main content -->
                <span class="return-up"><i class="fa fa-chevron-up"></i></span>
            </div>
            <!-- /. content-wrapper -->
            <!-- Main Footer -->
            <footer class="main-footer">
        <!-- Default to the left -->
        <strong>Copyright &copy; 2018 <a href="#">CMIS</a>.</strong> All rights reserved.
        <!-- To the right -->
        <div class="pull-right hidden-xs"></div>
        </footer>
        </div>

        <!-- /. wrapper content-->
        <!-- JS scripts -->
        <script src="vendor/jQuery/jquery-2.2.3.min.js"></script>
        <script src="vendor/jquery-fullscreen/jquery.fullscreen-min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/fastclick/fastclick.min.js"></script>
        <script src="vendor/pickadate/picker.js"></script>
        <script src="vendor/pickadate/picker-date.js"></script>
        <script src="vendor/dropzone/dropzone.js"></script>
        <script src="resources/js/pages/jquery-pickadate.js"></script>
        <script src="resources/js/app.min.js"></script>
        <script src="resources/js/demo.js"></script>
        <!-- Slimscroll is required when using the fixed layout. -->
    </body>

</html>