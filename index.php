
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CMIS | System</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="resources/img/favicon.ico" />
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="vendor/iCheck/all.css" /> 

        <!-- Icons -->
        <link href="resources/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="resources/icons/themify-icons/themify-icons.css" rel="stylesheet">

        <!-- Theme style -->
        <link rel="stylesheet" href="resources/css/main-style.min.css">
        <link rel="stylesheet" href="resources/css/skins/all-skins.min.css">
        <link rel="stylesheet" href="resources/css/demo.css">

</head>
    <body class="skin-blue login-page">
        <div class="box-login">
            <div class="box-login-body">
                <h3><span><b>CMI-</b>System</span></h3>
                <p class="box-login-msg">Login</p>

            <?php
                    ob_start();
                    session_start();
                    include 'conn.php';
                    

                    if(isset($_SESSION['userId'])){

                        header('location:dashboard.php');
                    }

                    if(isset($_POST['submit'])){

                        $email = trim(strip_tags($_POST['email']));
                        $pwd =  trim(strip_tags($_POST['pwd']));
                        $encrypted_md5_password = md5($pwd);

                        // $pwd=stripslashes($pwd);
                        $email=stripslashes($email);
                        $encrypted_md5_password =stripslashes($encrypted_md5_password);

                        // $pwd=mysql_real_escape_string($pwd);
                        $email=mysql_real_escape_string($email);
                        $encrypted_md5_password =mysql_real_escape_string($encrypted_md5_password);

                        $validate_user_information = mysql_query("select * from users where email='$email' and password='$encrypted_md5_password'");

                        $result = mysql_num_rows($validate_user_information);

                        if(mysql_num_rows($validate_user_information) == 0) // User not found. So, redirect to login_form again.
                            {
                                header('Location: index.php');
                            }
                             
                            $userData = mysql_fetch_array($validate_user_information, MYSQL_ASSOC);


                        if($userData['status'] == 'admin' )
                            {
                               
                                session_regenerate_id();
                                $_SESSION['userId'] = $userData['userId'];
                                $_SESSION['username'] = $userData['username'];
                                $_SESSION['email'] = $userData['email'];
                                $_SESSION['status'] = $userData['status'];
                                session_write_close();

                               
                                header("location: dashboard.php");
                                exit;
                                
                            }

                             elseif($userData['status'] == 'finance' )
                            {
                               
                                session_regenerate_id();
                                $_SESSION['userId'] = $userData['userId'];
                                $_SESSION['username'] = $userData['username'];
                                $_SESSION['email'] = $userData['email'];
                                $_SESSION['status'] = $userData['status'];
                                session_write_close();

                               
                                header("location: finance-dashboard.php");
                                exit;
                                
                            }

                             elseif($userData['status'] == 'society' )
                            {
                               
                                session_regenerate_id();
                                $_SESSION['userId'] = $userData['userId'];
                                $_SESSION['username'] = $userData['username'];
                                $_SESSION['email'] = $userData['email'];
                                $_SESSION['status'] = $userData['status'];
                                session_write_close();

                               
                                header("location: society-dashboard.php");
                                exit;
                                
                            }
                            else{
                                echo "<center><p style=color:red;>Invalid Email or Password</p></center>";

                        }
                        
                    }
                ?>

                <form class="login-form" action="" method="post">

              

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>

                        <input class="form-control" type="email" name='email' placeholder="Email" autofocus/>
                    </div>

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>

                        <input class="form-control" type="password" name='pwd' placeholder="Password" />
                    </div>
                    <div class="form-group input-group">
                        <div class="checkbox">
                            <label for="terms" style="padding-left: 12px;">
                                <input class="icheck_flat_20" type="checkbox" id="terms"> Remember Me
                            </label>
                        </div>
                    </div>

                    <div class="form-group form-action">
                        <button type="submit" name="submit" id="submit" class="btn btn-block btn-primary"> Sign In</button>
                    </div>

                    

                    <div style="margin-top:10px; margin-bottom: 10px; text-align:center;"><a href="sign-up.php">Register</a> if you don't have account</div>
                    <div class="form-group text-center">
                        <a href="forgot-password.html">Forgot Password</a>&nbsp;|&nbsp;<a href="#">Support</a>
                    </div>
                </form>
            </div>
        </div>
		
        <!-- JS scripts -->
        <script src="vendor/jQuery/jquery-2.2.3.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="resources/js/pages/jquery-icheck.js"></script>
        <script src="vendor/fastclick/fastclick.min.js"></script>
		<script src="resources/js/demo.js"></script>
    </body>
</html>